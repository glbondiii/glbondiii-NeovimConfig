local function find_lsp_root()
	-- Get lsp client for current buffer
	-- Returns nil or string
	local buf_ft = vim.api.nvim_get_option_value("filetype", { buf = 0 })
	local clients = vim.lsp.get_clients({ bufnr = 0 })
	if clients == nil or next(clients) == nil then
		return nil
	end

	for _, client in pairs(clients) do
		local filetypes = client.config.filetypes
		if filetypes and vim.tbl_contains(filetypes, buf_ft) then
			return client.config.root_dir
		end
	end

	return nil
end

local function find_pattern_root()
	-- Array of file names indicating root directory. Modify to your liking.
	local root_names = { ".git", ".rootdir" }

	-- Get directory path to start search from
	local path = vim.api.nvim_buf_get_name(0)
	if path == "" then
		return nil
	end
	path = vim.fs.dirname(path)

	local root_file = vim.fs.find(root_names, { path = path, upward = true })[1]
	if root_file == nil then
		return nil
	end

	return vim.fs.dirname(root_file)
end

local function set_root()
	local root = find_lsp_root()

	if root == nil then
		root = find_pattern_root()
	end

	if root ~= nil then
		vim.api.nvim_set_current_dir(root)
	end
end

local root_augroup = vim.api.nvim_create_augroup("MyAutoRoot", {})
vim.api.nvim_create_autocmd("BufEnter", { group = root_augroup, callback = set_root })
