TelescopeOpts = {
	defaults = {
		initial_mode = "normal",
	},
	pickers = {
		find_files = {
			find_command = { "rg", "--files", "--hidden", "--glob", "!**/.git/*" },
		},
		live_grep = {
			initial_mode = "insert",
		},
	},
}

function TelescopeKeys()
	local builtin = require("telescope.builtin")
	return {
		{
			"<leader>ff",
			function()
				builtin.find_files()
			end,
		},
		{
			"<leader>fg",
			function()
				builtin.live_grep()
			end,
		},
		{
			"<leader>fd",
			function()
				builtin.find_files({ cwd = vim.fn.expand("%:p:h") })
			end,
		},
		{
			"<leader>fs",
			function()
				builtin.live_grep({ cwd = vim.fn.expand("%:p:h") })
			end,
		},
		{
			"<leader>fa",
			function()
				builtin.buffers()
			end,
		},
	}
end

require("telescope").setup(TelescopeOpts)

local telescope_keys = TelescopeKeys()
for _, keybind in pairs(telescope_keys) do
	vim.keymap.set("n", keybind[1], keybind[2])
end
