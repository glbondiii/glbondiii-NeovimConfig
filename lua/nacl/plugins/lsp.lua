local java_ver_set = "JavaSE-21" -- Current LTS Java Goes Here
local java_path_set = "path/to/openjdk"
local omnisharp_cmd_set = { "path/to/omnisharp/bin/OmniSharp" }

function LspConfig(java_ver, java_path, omnisharp_cmd)
	-- Reserve a space in the gutter
	vim.opt.signcolumn = "yes"

	-- Add cmp_nvim_lsp capabilities settings to lspconfig
	-- This should be executed before you configure any language server
	local lspconfig_defaults = require("lspconfig").util.default_config
	lspconfig_defaults.capabilities =
		vim.tbl_deep_extend("force", lspconfig_defaults.capabilities, require("cmp_nvim_lsp").default_capabilities())

	-- Set single file support to true by default for all LSPs
	lspconfig_defaults.single_file_support = true

	-- This is where you enable features that only work
	-- if there is a language server active in the file
	vim.api.nvim_create_autocmd("LspAttach", {
		desc = "LSP actions",
		callback = function(event)
			local opts = { buffer = event.buf }

			vim.keymap.set("n", "gd", function()
				vim.lsp.buf.definition()
			end, opts)
			vim.keymap.set("n", "K", function()
				vim.lsp.buf.hover()
				-- Click K again to enter hover window
				-- Click q to exit hover window
			end, opts)
			vim.keymap.set("i", "<C-h>", function()
				vim.lsp.buf.signature_help()
			end, opts)
			vim.keymap.set("n", "[d", function()
				vim.diagnostic.goto_prev()
			end, opts)
			vim.keymap.set("n", "]d", function()
				vim.diagnostic.goto_next()
			end, opts)
			vim.keymap.set("n", "<leader>sd", function() -- i.e "show diagnostic" (as a float window)
				vim.diagnostic.open_float()
			end, opts)
			vim.keymap.set("n", "<leader>ca", function()
				vim.lsp.buf.code_action()
			end, opts)
			vim.keymap.set("n", "<leader>vr", function()
				vim.lsp.buf.references()
			end, opts)
			vim.keymap.set("n", "<leader>rn", function()
				-- when rename opens the prompt, this autocommand will trigger
				-- it will "press" CTRL-F to enter the command-line window `:h cmdwin`
				-- in this window I can use normal mode keybindings
				vim.api.nvim_create_autocmd({ "CmdlineEnter" }, {
					callback = function()
						local key = vim.api.nvim_replace_termcodes("<C-f>", true, false, true)
						vim.api.nvim_feedkeys(key, "c", false)
						vim.api.nvim_feedkeys("0", "n", false)
						return true
					end,
				})
				vim.lsp.buf.rename()
			end, opts)
			vim.keymap.set("n", "<leader>ws", function()
				vim.lsp.buf.workspace_symbol()
			end, opts)
			vim.keymap.set("n", "<leader>cf", function()
				require("conform").format()
			end, opts)
		end,
	})

	-- Borders for LSP Boxes
	vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, { border = "rounded" })
	vim.lsp.handlers["textDocument/signatureHelp"] =
		vim.lsp.with(vim.lsp.handlers.signature_help, { border = "rounded" })
	vim.diagnostic.config({
		float = { border = "rounded" },
	})

	-- Mason Setup
	--[[
  require('mason').setup({})
  require('mason-lspconfig').setup({
    -- Replace the language servers listed here 
    -- with the ones you want to install
    ensure_installed = {},
    handlers = {
      function(server_name)
        require('lspconfig')[server_name].setup({})
      end,
    },
  }

  ]]

	-- Manual Language Server and Formatter Setup
	require("lspconfig").clangd.setup({}) -- C, C++, Obj-C, Obj-C++
	require("lspconfig").lua_ls.setup({}) -- Lua
	require("lspconfig").nixd.setup({}) -- Nix
	require("lspconfig").pyright.setup({}) -- Python
	require("lspconfig").vtsls.setup({}) -- JavaScript, TypeScript, React
	-- Begin VSCode Servers
	require("lspconfig").html.setup({}) -- HTML
	require("lspconfig").cssls.setup({}) -- CSS
	require("lspconfig").eslint.setup({}) -- ESLint
	require("lspconfig").jsonls.setup({}) -- JSON
	-- No Markdown LSP Config :(
	-- End VSCode Servers
	require("lspconfig").gopls.setup({}) -- Go
	require("lspconfig").solargraph.setup({}) -- Ruby
	require("lspconfig").hls.setup({}) -- Haskell
	require("lspconfig").texlab.setup({}) -- TeX

	-- Rust
	require("lspconfig").rust_analyzer.setup({
		settings = {
			["rust-analyzer"] = {
				check = {
					command = "clippy",
				},
			},
		},
	})

	-- Java
	require("lspconfig").jdtls.setup({
		settings = {
			java = {
				configuration = {
					runtimes = {
						{
							name = java_ver,
							path = java_path,
							default = true,
						},
					},
				},
			},
		},
	})

	-- C#
	require("lspconfig").omnisharp.setup({
		cmd = omnisharp_cmd,
	})
end

-- LspConfig(java_ver_set, java_path_set, omnisharp_cmd_set)
