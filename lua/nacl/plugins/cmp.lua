function CmpOpts()
	local cmp = require("cmp")
	local cmp_select = { behavior = cmp.SelectBehavior.Select }

    -- Reset <C-p> and <C-n> keymaps
    vim.keymap.set("i", "<C-p>", "<Nop>")
    vim.keymap.set("i", "<C-n>", "<Nop>")

	return {
		sources = cmp.config.sources({
			{ name = "nvim_lsp" },
			{ name = "nvim_lsp_signature_help" },
			{ name = "luasnip" },
			{ name = "buffer" },
		}),
		snippet = {
			expand = function(args)
				require('luasnip').lsp_expand(args.body)
			end,
		},
		window = {
			completion = cmp.config.window.bordered({}),
			documentation = cmp.config.window.bordered({}),
		},
		mapping = cmp.mapping.preset.insert({
			["<C-p>"] = cmp.mapping.scroll_docs(-4),
			["<C-n>"] = cmp.mapping.scroll_docs(4),
			["<C-Space>"] = cmp.mapping.complete(),
			["<C-e>"] = cmp.mapping.abort(),
			["<C-s>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
			["<C-k>"] = cmp.mapping(function()
				if cmp.visible() then
					cmp.select_prev_item(cmp_select)
				else
					cmp.complete()
				end
			end),
			["<C-j>"] = cmp.mapping(function()
				if cmp.visible() then
					cmp.select_next_item(cmp_select)
				else
					cmp.complete()
				end
			end),
		}),
	}
end

require("cmp").setup(CmpOpts())
