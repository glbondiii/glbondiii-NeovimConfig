--Substitute <leader> tag for this value
vim.g.mapleader = " "

--Go to Neovim File Manager/Finder (net-rw) when looking out of
--current directory
vim.keymap.set("n", "<leader>fv", vim.cmd.Ex)

-- Quick Buffer Operations
vim.keymap.set("n", "<C-h>", vim.cmd.bprev)
vim.keymap.set("n", "<C-l>", vim.cmd.bnext)
vim.keymap.set("n", "<leader>Q", vim.cmd.bdelete)

-- Local Working Directory Traversal (Overwritten upon leaving buffer (and maybe before))
vim.keymap.set("n", "<leader>cd", [[:lcd %:h<CR>]])
vim.keymap.set("n", "<leader>ed", [[:lcd -<CR>]])

-- Vim Jump Remap
vim.keymap.set("n", "<C-d>", "<C-d>zz")
vim.keymap.set("n", "<C-u>", "<C-u>zz")

--Uses "J" and "K" to correspondingly adjust highlighted
--text
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- Better Keybinds for Letter Find
vim.keymap.set("n", "<Tab>", ";")
vim.keymap.set("n", "<S-Tab>", ",")

--Allows cursor to stay in middle when searching for terms
vim.keymap.set("n", "n", "nzzzv")
vim.keymap.set("n", "N", "Nzzzv")

--Preserves previous saved content when pasting over or
--deleting text
--called the "greatest remap ever"
vim.keymap.set("x", "<leader>rp", '"_dP')
vim.keymap.set("n", "<leader>d", '"_d')
vim.keymap.set("v", "<leader>d", '"_d')

--Allows to copy Vim content to clipboard if desired
vim.keymap.set("n", "<leader>y", '"+y')
vim.keymap.set("v", "<leader>y", '"+y')
vim.keymap.set("n", "<leader>Y", '"+Y')
vim.keymap.set("n", "<leader>p", '"+p')
vim.keymap.set("v", "<leader>p", '"+p')
vim.keymap.set("n", "<leader>P", '"+P')

-- Find and Replace Current Highlighted Word
vim.keymap.set("n", "<leader>rw", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- Fix Indenting For File (Only use if no formatter)
vim.keymap.set("n", "<leader>ci", "gg=G")

-- Create Terminal Buffer in Current File Working Directory
vim.keymap.set("n", "<leader>tt", function()
	vim.cmd("lcd %:h")
	vim.cmd("terminal")
	vim.cmd("startinsert")
	vim.cmd("lcd -")
end)

-- Create Terminal Buffer in Project Root Directory (Semi-Default Behavior)
vim.keymap.set("n", "<leader>tr", vim.cmd.terminal)

--Exit Terminal Insert Mode With Esc
vim.keymap.set("t", "<Esc>", "<C-\\><C-n>")

-- Exit Command Line Mode With Esc
vim.api.nvim_create_autocmd({ "CmdwinEnter" }, {
	callback = function()
		vim.keymap.set("n", "<Esc>", ":quit<CR>", { buffer = true })
	end,
})

-- QuickFix Keybinds
vim.keymap.set("n", "<C-k>", ":cprevious<CR>")
vim.keymap.set("n", "<C-j>", ":cnext<CR>")
vim.keymap.set("n", "<C-q>", ":cclose<CR>")
