# NaCl-NeovimConfig
Originally based on ThePrimagen's Neovim config (see ThePrimagen/init.lua)

Installation Instructions (Prerequisites: Install ripgrep, fd, and a terminal-accessible clipboard; 
if using Nix, NixOS or Home Manager, these are already provided for you)
1. Clone this repository into the directory where your config files are kept (usually ~/.config on Linux/MacOS(?); 
C:\Users\<username>\AppData\Local on Windows)
    * If running NixOS or using Nix and/or Home Manager, install [nixvim](https://nix-community.github.io/nixvim/user-guide/install.html), 
    import nixvim.nix into your corresponding configuration file, and skip to step 4
2. Rename the "NaCl-NeovimConfig" directory to "nvim"
3. Go into lua/nacl/init.lua and uncomment the last line to opt into using lazy.nvim
4. Troubleshoot if anything goes wrong with plugins
5. Profit.
