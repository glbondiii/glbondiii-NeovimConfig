local java_ver_set = "JavaSE-21" -- Current LTS Java Goes Here
local java_path = "path/to/openjdk"
local omnisharp_cmd = { "path/to/omnisharp/bin/OmniSharp" }

return {
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			"hrsh7th/cmp-nvim-lsp",
			"stevearc/conform.nvim",
			-- Do not include Mason plugins if using NixOS or otherwise manually setting up language servers
			--"williamboman/mason.nvim",
			--"williamboman/mason-lspconfig.nvim",
		},
		config = require("plugins.lsp").LspConfig(java_ver_set, java_path, omnisharp_cmd),
	},
	{
		"hrsh7th/nvim-cmp",
		dependencies = { "hrsh7th/cmp-nvim-lsp", "hrsh7th/cmp-buffer" },
		opts = require("plugins.cmp").CmpOpts(),
	},
	{
		"stevearc/conform.nvim",
		opts = require("plugins.format").ConformOpts,
	},
	{
		"nvim-flutter/flutter-tools.nvim",
		lazy = false,
		dependencies = {
			"nvim-lua/plenary.nvim",
		},
		config = function()
			require("flutter-tools").setup({}) -- use defaults
		end,
	},
	{
		"folke/tokyonight.nvim",
		lazy = false,
		priority = 1000,
    opts = {
      style = "night",
      transparent = true,
    },
	},
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		dependencies = { "nvim-treesitter/treesitter-context" },
		config = require("plugins.treesitter").TreesitterConfig(),
	},
	{
		"nvim-telescope/telescope.nvim",
		branch = "0.1.x",
		dependencies = { "nvim-lua/plenary.nvim" },
		opts = require("plugins.telescope").TelescopeOpts,
		keys = require("plugins.telescope").TelescopeKeys(),
	},
	{
		"nvim-lualine/lualine.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		opts = require("plugins.lualine").LualineOpts,
	},
	{
		"j-hui/fidget.nvim",
		opts = require("plugins.fidget-opts").FidgetOpts,
	},
	{
		"nacl-gb3/nacl-multicolumn.nvim",
		opts = require("plugins.multicolumn").MultiColOpts,
	},
	{
		"christoomey/vim-tmux-navigator",
		cmd = require("plugins.vim-tmux-nav").VimTmuxNavCmd,
		keys = require("plugins.vim-tmux-nav").VimTmuxNavKeysVim,
	},
}
