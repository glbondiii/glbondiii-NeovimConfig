MultiColOpts = {
	start = "enabled", -- enabled, disabled, remember
	update = "on_move", -- on_move, lazy_hold, int
	command = "multiple", -- multiple, single, none
	max_lines = 0, -- 0 (disabled) OR int
	max_size = 0, -- 0 (disabled) OR int
	use_default_set = true,
	exclude_floating = true,
	exclude_ft = {},
	editorconfig = true,
	base_set = {
		scope = "line", -- file, window, line
		rulers = {}, -- { int, int, ... }
		to_line_end = false,
		full_column = true,
		always_on = false,
		on_exceeded = true,
		bg_color = nil,
		fg_color = nil,
	},
	sets = {
		default = {
			rulers = { 0 },
		},
		c = {
			rulers = { 80 },
		},
		cpp = {
			rulers = { 80 },
		},
		lua = {
			rulers = { 120 },
		},
		python = {
			rulers = { 88 },
		},
		ruby = {
			rulers = { 80 },
		},
		haskell = {
			rulers = { 80 },
		},
		rust = {
			rulers = { 100 },
		},
		java = {
			rulers = { 100 },
		},
		cs = {
			rulers = { 65 },
		},
	},
}

require("multicolumn").setup(MultiColOpts)
