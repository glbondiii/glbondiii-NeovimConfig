--General Settings(Number, Indent, etc.)
vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.scrolloff = 8
vim.opt.smartindent = true
vim.opt.wrap = false

--Programming Style Settings
-- Default Tab Settings
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2
vim.opt.expandtab = true
vim.opt.shiftround = true

-- C
vim.g.c_syntax_for_h = 1

-- Python
vim.g.python_exec = 1

-- Rust
vim.g.rust_exec = 1

-- Go
vim.g.go_exec = 1

-- Markdown
vim.g.markdown_exec = 1

-- Java
vim.g.java_exec = 1

-- Additional Style Settings in after/ftplugin directory

--Search Highlight Settings
vim.opt.hlsearch = false
vim.opt.incsearch = true

-- Various Settings
vim.opt.updatetime = 50
vim.opt.ttimeoutlen = 100 --in milliseconds

--Theme/Coloring Settings
vim.opt.termguicolors = true
