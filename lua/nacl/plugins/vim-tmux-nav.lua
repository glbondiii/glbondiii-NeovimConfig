VimTmuxNavCmd = {
	"TmuxNavigateLeft",
	"TmuxNavigateDown",
	"TmuxNavigateUp",
	"TmuxNavigateRight",
}

VimTmuxNavKeysVim = {
	{ "<M-h>", "<cmd><C-U>TmuxNavigateLeft<cr>" },
	{ "<M-j>", "<cmd><C-U>TmuxNavigateDown<cr>" },
	{ "<M-k>", "<cmd><C-U>TmuxNavigateUp<cr>" },
	{ "<M-l>", "<cmd><C-U>TmuxNavigateRight<cr>" },
}

VimTmuxNavKeysLua = {
	{ "<M-h>", vim.cmd.TmuxNavigateLeft },
	{ "<M-j>", vim.cmd.TmuxNavigateDown },
	{ "<M-k>", vim.cmd.TmuxNavigateUp },
	{ "<M-l>", vim.cmd.TmuxNavigateRight },
}

function VimTmuxNavConf()
	vim.g.tmux_navigator_no_mappings = 1

	for _, keybind in pairs(VimTmuxNavKeysLua) do
		vim.keymap.set("n", keybind[1], keybind[2])
	end
end

VimTmuxNavConf()
