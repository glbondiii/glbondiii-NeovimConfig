LIGHT_MODE_ON = false
NUMBER_HIGHLIGHT_ON = false

function ToggleLightMode()
	if LIGHT_MODE_ON then
		vim.cmd.colorscheme("tokyonight-night")
		if NUMBER_HIGHLIGHT_ON then
			vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "white" })
			vim.api.nvim_set_hl(0, "LineNr", { fg = "white" })
			vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "white" })
		end
		LIGHT_MODE_ON = false
	else
		vim.cmd.colorscheme("tokyonight-day")
		if NUMBER_HIGHLIGHT_ON then
			vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "black" })
			vim.api.nvim_set_hl(0, "LineNr", { fg = "black" })
			vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "black" })
		end
		LIGHT_MODE_ON = true
	end
end

function HighlightNumbers()
	if NUMBER_HIGHLIGHT_ON then
		if LIGHT_MODE_ON then
			vim.cmd.colorscheme("tokyonight-day")
		else
			vim.cmd.colorscheme("tokyonight-night")
		end
		NUMBER_HIGHLIGHT_ON = false
	else
		if LIGHT_MODE_ON then
			vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "black" })
			vim.api.nvim_set_hl(0, "LineNr", { fg = "black" })
			vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "black" })
		else
			vim.api.nvim_set_hl(0, "LineNrAbove", { fg = "white" })
			vim.api.nvim_set_hl(0, "LineNr", { fg = "white" })
			vim.api.nvim_set_hl(0, "LineNrBelow", { fg = "white" })
		end
		NUMBER_HIGHLIGHT_ON = true
	end
end

vim.keymap.set("n", "<leader>lm", function()
	ToggleLightMode()
end)

vim.keymap.set("n", "<leader>hl", function()
	HighlightNumbers()
end)
