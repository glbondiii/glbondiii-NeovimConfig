ConformOpts = {
	formatters_by_ft = {
		c = { "clang-format" },
		cpp = { "clang-format" },
		lua = { "stylua" },
		nix = { "nixfmt" },
		python = { "black" },
		go = { "gofmt" },
		haskell = { "fourmolu" },
		rust = { "rustfmt" },
	},
	default_format_opts = {
		lsp_format = "fallback",
	},
}

require("conform").setup(ConformOpts)
