{ config, pkgs, ... }:
let
  nacl-multicolumn = pkgs.vimUtils.buildVimPlugin {
    name = "nacl-multicolumn.nvim";
    src = pkgs.fetchFromGitHub {
      owner = "nacl-gb3";
      repo = "nacl-multicolumn.nvim";
      rev = "bd6badae7a2b179a7edaa32ccb46e434e2a8004e";
      hash = "sha256-cZukPfGjmB38SCkntHSqZwwl/gUX35aRNSE/N5TYz3o=";
    };
  };
in
{
  programs.nixvim = {
    enable = true;

    extraConfigLuaPre = ''
      ${builtins.readFile ./lua/nacl/set.lua}

      ${builtins.readFile ./lua/nacl/remap.lua}
    '';

    extraConfigLua = ''
      ${builtins.readFile ./lua/nacl/plugins/lsp.lua}
      -- Match Nix LTS Version of Java
      LspConfig("JavaSE-21", "${pkgs.jdk}/lib/openjdk", { '${pkgs.omnisharp-roslyn}/bin/OmniSharp' })

      ${builtins.readFile ./lua/nacl/plugins/snippets.lua}

      ${builtins.readFile ./lua/nacl/plugins/cmp.lua}

      ${builtins.readFile ./lua/nacl/plugins/format.lua}

      require('flutter-tools').setup {}

      ${builtins.readFile ./lua/nacl/plugins/treesitter.lua}

      ${builtins.readFile ./lua/nacl/plugins/telescope.lua}

      ${builtins.readFile ./lua/nacl/plugins/lualine.lua}

      ${builtins.readFile ./lua/nacl/plugins/fidget-opts.lua}

      ${builtins.readFile ./lua/nacl/plugins/multicolumn.lua}

      ${builtins.readFile ./lua/nacl/plugins/vim-tmux-nav.lua}

    '';

    extraConfigLuaPost = ''
      ${builtins.readFile ./lua/nacl/colors.lua}
      ${builtins.readFile ./lua/nacl/find-root.lua}
    '';

    extraFiles = {
      "after/ftplugin/c.lua".text = ''${builtins.readFile ./after/ftplugin/c.lua}'';
      "after/ftplugin/cpp.lua".text = ''${builtins.readFile ./after/ftplugin/cpp.lua}'';
      "after/ftplugin/lua.lua".text = ''${builtins.readFile ./after/ftplugin/lua.lua}'';
      "after/ftplugin/nix.lua".text = ''${builtins.readFile ./after/ftplugin/nix.lua}'';
      # Python ftplugin from Neovim
      "after/ftplugin/javascript.lua".text = ''${builtins.readFile ./after/ftplugin/javascript.lua}'';
      "after/ftplugin/typescript.lua".text = ''${builtins.readFile ./after/ftplugin/typescript.lua}'';
      "after/ftplugin/javascriptreact.lua".text = ''${builtins.readFile ./after/ftplugin/javascriptreact.lua}'';
      "after/ftplugin/typescriptreact.lua".text = ''${builtins.readFile ./after/ftplugin/typescriptreact.lua}'';
      "after/ftplugin/html.lua".text = ''${builtins.readFile ./after/ftplugin/html.lua}'';
      "after/ftplugin/css.lua".text = ''${builtins.readFile ./after/ftplugin/css.lua}'';
      "after/ftplugin/json.lua".text = ''${builtins.readFile ./after/ftplugin/json.lua}'';
      # Go ftplugin from Neovim
      "after/ftplugin/ruby.lua".text = ''${builtins.readFile ./after/ftplugin/ruby.lua}'';
      "after/ftplugin/haskell.lua".text = ''${builtins.readFile ./after/ftplugin/haskell.lua}'';
      # Rust ftplugin from Neovim
      "after/ftplugin/java.lua".text = ''${builtins.readFile ./after/ftplugin/java.lua}'';
      "after/ftplugin/cs.lua".text = ''${builtins.readFile ./after/ftplugin/cs.lua}'';
    };

    colorschemes.tokyonight = {
      enable = true;
      settings = {
        style = "night";
        transparent = false; # Set to true if making terminal transparent through app
      };
    };

    extraPlugins = with pkgs.vimPlugins; [
      # LSP and Formatting
      nvim-lspconfig
      luasnip
      nvim-cmp
      cmp-nvim-lsp
      cmp-nvim-lsp-signature-help
      cmp_luasnip
      cmp-buffer
      conform-nvim
      friendly-snippets
      flutter-tools-nvim

      # Other Plugins
      nvim-treesitter.withAllGrammars
      nvim-treesitter-context
      telescope-nvim
      lualine-nvim
      nvim-web-devicons # dependency of Lualine
      fidget-nvim
      nacl-multicolumn
      vim-tmux-navigator
    ];

    extraPackages = with pkgs; [
      wl-clipboard
      ripgrep
      fd
      tree-sitter

      # Language Servers and Linters
      clang-tools # C, C++, Obj-C, Obj-C++
      lua-language-server # Lua
      nixd # Nix
      pyright # Python
      vtsls # Javascript, Typescript, React
      vscode-langservers-extracted # HTML, CSS, ESLint, JSON; Markdown Server Unused :(
      gopls # Go
      solargraph # Ruby
      haskell-language-server # Haskell
      texlab # TeX

      rust-analyzer # Rust
      clippy # Rust (Linter)

      jdt-language-server # Java
      jdk # For jdt-language server

      omnisharp-roslyn # C#

      # Formatters
      # clang-format provided by clang-tools
      stylua # Lua
      nixfmt-rfc-style # Nix
      black # Python
      # gofmt provided by go
      haskellPackages.fourmolu # Haskell
      rustfmt # Rust
    ];
  };
}
